<?php

namespace Drupal\a11y_tableau\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * A11y Tableau Form.
 */
class A11yTableauForm extends FormBase implements FormInterface {

  /**
   * The session.
   *
   *   Private SessionInterface $session;
   */
  private SessionInterface $session;

  /**
   * Constructs a new AuthService object.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   */
  public function __construct(SessionInterface $session) {
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $self = new self(
      $container->get('session'),
    );
    return $self;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'a11y_tableau_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['embedded_link'] = [
      '#type' => 'textfield',
      '#title' => 'Embedded Link',
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // $embedded_link = $form_state->getValue('embedded_link');
    // Add validation if required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Form submission logic goes here.
    $embeddedLink = $form_state->getValue('embedded_link');

    // Store the embedded link in a session for later use.
    $this->session->set('a11y_tableau_embedded_link', $embeddedLink);

    // Redirect to graph page.
    $form_state->setRedirect('a11y_tableau.graph');
    // Log the embedded link for debugging
    // $this->logger->debug('Embedded link: @link', ['@link' => $embeddedLink]);
    // Process the embedded link and retrieve the graph
    // (This will depend on the specific method you
    // use to retrieve the graph)
    // Add the graph to the form result
    // $form_state->set('graph', $graph);.
  }

}

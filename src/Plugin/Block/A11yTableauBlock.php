<?php

namespace Drupal\a11y_tableau\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Exposes a11y_tableau as block.
 *
 * @Block(
 *      id = "tableau_block",
 *      admin_label = @Translation("a11y_tableau"),
 *      category = @Translation("Custom block for custom tableau")
 * )
 */
class A11yTableauBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#title' => 'My Graph',
      '#theme' => 'a11y_tableau_graph',
      '#item' => 'Hello world',
    ];

  }

}

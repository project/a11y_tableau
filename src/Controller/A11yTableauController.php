<?php

namespace Drupal\a11y_tableau\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A11y Tableau Controller.
 */
class A11yTableauController extends ControllerBase {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * A11yTableauController constructor.
   *
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(FormBuilder $formBuilder, LoggerInterface $logger) {
    $this->formBuilder = $formBuilder;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('logger.factory')->get('a11y_tableau')
    );
  }

  /**
   * Displays the graph using Tableau.
   */
  public function displayGraph() {
    return [
      '#title' => 'My Graph',
      '#theme' => 'a11y_tableau_graph',
      '#item' => 'Hello world',
    ];
  }

}
